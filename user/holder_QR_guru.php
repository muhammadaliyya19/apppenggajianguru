<?php 
session_start();
	include 'header_user.php'; 
?>
<div class="container">

	<div class="row">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">
					Scan QR Ini Untuk Absensi
				</h3>
			</div>
			<div class="alert alert-info">  
				<?php
				echo date('l, d-m-Y');
				?>
			</div>   
			<!-- Holder IMAGE nya -->
			<div align="center">
				<h4><?= $_GET['nik'] . " - ". $_GET['nama'];?> </h4>
				<a href="<?php echo $_SESSION['txtQR'];?>" target="_blank"><img src="<?= $_GET['src'];?>"/></a><br>
				<a href="absen_guru.php" class="btn btn-info"> Kembali ke Absensi </a>
				<?php 
					unset($_SESSION['txtQR']);
				?>
				<br>
				<br>
			</div>
		</div>
	</div>
</div>




</div>
</div>
</div>

</div>
<?php include "footer_user.php"; ?>

